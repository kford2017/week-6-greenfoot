import greenfoot.*;

/**
 * A very simple fly actor that walks around in a fly-like manner.
 * 
 * @author (BE SURE TO PUT YOUR NAME HERE! You will lose points if not)
 * @version (PUT THE DATE YOU FINISH HERE)
 */
public class Fly extends Actor
{
    //Fields that can be used anywhere in the Fly class
    private int waitCount, turnAmount, moveAmount, worldCenterX, worldCenterY;
    
    /**
     * Constructor to be used to initialize the fields declared above
     */
    public Fly()
    {
        //When a fly is created, we want to start the wait counter to 0
        waitCount = 0;
        
        //Set the coordinates for the center of BugWorld
        worldCenterX = BugWorld.WORLD_WIDTH / 2;
        worldCenterY = BugWorld.WORLD_HEIGHT / 2;
    }

    /**
     * Act - do whatever the Fly wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //Make the fly move more slowly by having it wait to move every 5 times the act method is called
        if(waitCount < 5)
        {
            //Increase the wait counter by 1 and just sit tight until waitCount reaches 5
            waitCount++;
        }
        else
        {
            //It's time to move! Turn the fly a random number of degrees between -90 and 90. Then move a random number
            //of steps between 1 and 12 inclusive
            turnAmount = Greenfoot.getRandomNumber(91) - 90;
            moveAmount = Greenfoot.getRandomNumber(12);
            turn(turnAmount);
            move(moveAmount);
            
            //See if the fly is stuck on an edge. If so, make it turn towards the center of the world
            //and move half the distance to that location
            if(isAtEdge())
            {
                //Turn towards the center of the world
                turnTowards(worldCenterX, worldCenterY);
                
                //Move to the center of the world. Must calculate the distance between the fly's current
                //location and the center of the world. Thank Pythagoras for the distance calculation!
                int distanceToCenter = (int)Math.sqrt(Math.pow(getX()-worldCenterX,2) + Math.pow(getY()-worldCenterY,2));
                move(distanceToCenter*2);
            }
            
            
            //Reset the wait counter
            waitCount = 0;
        }        

    }    
}
