import greenfoot.*;

/**
 * BugWorld demonstrates more sophisticated graphics handling as well as provides examples of while-loops
 * 
 * @author (BE SURE TO PUT YOUR NAME HERE! You will lose points if not)
 * @version (PUT THE DATE YOU FINISH HERE)
 */
public class BugWorld extends World
{
    //Fields for values that can be used anywhere in the class. Feel free to play with these!
    private final int NUMBER_OF_FLIES = 5;
    private final int NUMBER_OF_SPIDERS = Greenfoot.getRandomNumber(50);
    final static int WORLD_WIDTH = 600;
    final static int WORLD_HEIGHT = 400;
    
    //You will need to have a GreenfootImage object for the spider that is going to be drawn on the world's
    //background. Since more than one method needs to work with this image, you will need to declare the
    //reference variable here, and instantiate it in the world constructor.
    private GreenfootImage spider;
   
    /**
     * Constructor for objects of class BugWorld.
     */
    public BugWorld()
    {
        super(WORLD_WIDTH, WORLD_HEIGHT, 1);
        
        //Instantiate the spider GreenfootImage
        spider = new GreenfootImage("spider.png");
                
        drawSpidersOnBackground();
        populateWorldWithFlies();
        
    }

    /**
     * Draw as many spiders as determined by the NUMBER_OF_SPIDERS constant
     */
    private void drawSpidersOnBackground()
    {
        int buffer = spider.getWidth();
        
        int x, y;
        
        int count = 0;
        while(count < NUMBER_OF_SPIDERS)
        {
            x = Greenfoot.getRandomNumber(getWidth()-(2*buffer)) + buffer;
            y = Greenfoot.getRandomNumber(getHeight()-(2*buffer)) + buffer;
            drawSpiderAt(x,y);
            count++;
        }
    }
    
    /**
     * Place as many Fly objects in the world as determined by NUMBER_OF_FLIES
     */
    private void populateWorldWithFlies()
    {
        int count = 0;
        while(count < NUMBER_OF_FLIES)
        {
            placeFly();
            count++;
        }
    }
    
    /**
     * Draw a single spider on to the world's background image. The spider image will be rotated 90 degrees
     * from its current orientation and given a random transparency value before being drawn on the
     * background. This gives a little bit more visual interest to the world's background.
     * @param int x - x-coordinate for where to draw the spider
     * @param int y - y-coordinate for where to draw the spider
     */
    private void drawSpiderAt(int x, int y)
    {
        //Turn the spider image 90 degrees clockwise from its current orientation
        spider.rotate(90);
        
        //Make the spider image a random transparency value between 20 and 200 (inclusive)
        spider.setTransparency(Greenfoot.getRandomNumber(181)+20);
        
        //Finally, draw the spider image on the background image
        getBackground().drawImage(spider, x, y);
    }
    
    /**
     * Place a new Fly actor at a random location in the world. Turn the fly a random amount once it's been
     * placed.
     * @param int x - x-coordinate for where to place the fly
     * @param int y - y-coordinate for where to place the fly
     */
    private void placeFly()
    {
        //Create variables that will only be available in this method
        int x, y, buffer;
        int turnAmount = Greenfoot.getRandomNumber(360) - 180; //range: -180 to 179
        
        //Create a Fly actor to place in the world
        Fly fly = new Fly();
        
        //Set a buffer amount based on the Fly's image width value. When placing a Fly in the world we want
        //all parts of it to be visible.
        buffer = fly.getImage().getWidth();
        
        //Set the x and y coordinates for placing the fly in a random location in the world
        x = Greenfoot.getRandomNumber(getWidth()-(2*buffer)) + buffer;
        y = Greenfoot.getRandomNumber(getHeight()-(2*buffer)) + buffer;
        
        //Add the fly to the world
        addObject(fly,x,y);
        
        //Turn the fly by the random amount calculated earlier
        fly.turn(turnAmount);
    }
}
